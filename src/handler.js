const constants = require('../config/config').secrets;

const _greetings = () => `This is a very secure secret: ${constants.mySecret}`;

const getHandler = () => ({
  greetings: _greetings
});

module.exports.getHandler = getHandler;
