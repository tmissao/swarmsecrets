const fs = require("fs");

const secretsConfig = {
  SECRET_PATH: '/run/secrets/',
  MY_SECRET: 'MY-SECURE-SECRET'
};

const loadSecret = (secret) => {
  const { SECRET_PATH } = secretsConfig;
  try {
    return fs.readFileSync(`${SECRET_PATH}${secret}`, 'utf8')
  } catch (err) {
    throw err;
  }
}

const config = {
  secrets: {
    mySecret: loadSecret(secretsConfig.MY_SECRET).trim()
  }
};

module.exports = config;
