# Swarm Secret Demo

This is a simple project to demonstrates how to use and configure Swarm Secrets with Node.js and also facilitates local development.

### Usage

1. [Install Docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
2. Initialize Swarm
``` bash
docker swarm init
```
3. Create a secret

* It is possible to create a docker secret using the terminal
``` bash
echo "Hello There"| docker secret create MY-SECURE-SECRET -
```

* Or using a text file
``` bash
docker secret create MY-SECURE-SECRET mysecret.txt
```

4. Map the secret on docker-compose.yml file
``` yml
version: '3.3'
services:
  start-secret:
    image: secret-test
    secrets:
      - MY-SECURE-SECRET

 secrets:
   MY-SECURE-SECRET:
     external: true
```

5. Configure the Node.js app to read the secret from _**/run/secrets/{secretName}**_

``` javascript
const fs = require("fs");

const secretsConfig = {
  SECRET_PATH: '/run/secrets/',
  MY_SECRET: 'MY-SECURE-SECRET'
};

const loadSecret = (secret) => {
  const { SECRET_PATH } = secretsConfig;
  try {
    return fs.readFileSync(`${SECRET_PATH}${secret}`, 'utf8')
  } catch (err) {
    throw err;
  }
}

const config = {
  secrets: {
    mySecret: loadSecret(secretsConfig.MY_SECRET).trim()
  }
};
```

6. Build a Docker Image and deploy the stack using Swarm
``` bash
docker image build -t secret-test .
docker stack deploy -c docker-compose.yml myStack
```

7. See the container logs
``` bash
docker container logs containerID
```

### Development Environment

Since Secrets is a Swarm exclusive feature, it creates a problem during development environment, because docker-compose does not mount the directory **_run/secrets/secretName_**.

A good work-around for this problem is simulate the swarm secret using docker-compose and passing a file as secret. However it is important to know that this file is totally insecure, and it is just for development purpose.

1. [Install Docker-Compose](https://docs.docker.com/compose/install/)

2. Modify the docker-compose.yml file

``` yml
version: '3.3'
services:
  start-secret:
    build:
      context: .
    secrets:
      - MY-SECURE-SECRET

# This enables docker-compose to work with "secrets"
secrets:
  MY-SECURE-SECRET:
    file: ./secrets/mysecret.txt
```
3. Execute the docker-compose command.
``` bash
docker-compose up --build
```

That`s it Folks, Happy Code =)
