FROM node:9

# application placed into /opt/app
RUN mkdir -p /opt/app

WORKDIR /opt/app

# ENVIRONMENTS
ENV RUN_MODE ${RUN_MODE:-DEVELOPMENT}

# Install app dependencies
COPY package.json .
# For npm@5 or later, copy package-lock.json as well
# COPY package.json package-lock.json ./

RUN npm install --production && npm cache clean --force

COPY . .

CMD ["npm","start"]
